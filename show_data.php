<?php
    $DB_NAME = "medsos";
    $DB_USER = "root";
    $DB_PASS = "";
    $DB_SERVER_LOC = "localhost";

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $sql = "SELECT id, nama, perasaan, caption, concat('http://192.168.43.157/medsos/images/',foto) as url
                FROM posting";
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result)>0){
            header("Access-Control-Allow-Origin: *");
            header("Content-type: application/json; charset=UTF-8");

            $data_ms = array();
            while($ms = mysqli_fetch_assoc($result)){
                array_push($data_ms, $ms);
            }
            echo json_encode($data_ms);
        }
    }
?>