<?php
    session_start();
    $server_name = "localhost";
    $db_username = "root";
    $db_password = "";
    $db_name = "medsos";

    $connection = mysqli_connect($server_name,$db_username,$db_password,$db_name);
    $dbconfig = mysqli_select_db($connection,$db_name);    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Data</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
    integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js"
      integrity="sha384-xymdQtn1n3lH2wcu0qhcdaOpQwyoarkgLVxC/wZ5q7h9gHtxICrpcaSUfygqZGOe" crossorigin="anonymous">
    </script>
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link rel="stylesheet" href="style3.css">
    <style>
        .warning {
            color: #FF0000;
        }

        .image-preview {
          width: 300px;
          min-height: 100px;
          border: 2px solid #dddddd;
          margin-top: 15px;
          margin-left: 178px;
          
          display: flex;
          align-items: center;
          justify-content: center;
          font-weight: bold;
          color: #cccccc;
        }
        
        .image-preview__image {    
          width: 100%;
        }

        .image-preview__default-text{
          display: none;
        }
    </style>
</head>
<body>
  <div class="wrapper">
    <div class="title">
      Form Edit Data
    </div>
    <form action="code.php" method="post" enctype="multipart/form-data" class="form">
      <div class="inputfield">
        <label>Nama</label>
        <input type="text" class="input" placeholder="Masukkan Nama" name="nama" value="<?php echo $_SESSION['nama']; ?>">
      </div>
      <div class="inputfield">
        <label>Perasaan</label>
        <input type="radio" id="senang" name="perasaan" value="Senang" name="perasaan"
        <?php echo ($_SESSION['perasaan']=='Senang')?'checked':'' ?>>

        <label for=senang"
          style="float: left; clear: none; margin: 2px 0 0 2px; font-weight: unset;">&nbsp;Senang</label>

        <input type="radio" id="santai" name="perasaan" value="Santai" name="perasaan"
        <?php echo ($_SESSION['perasaan']=='Santai')?'checked':'' ?>>

        <label for="santai"
          style="float: left; clear: none; margin: 2px 0 0 2px; font-weight: unset;">&nbsp;Santai</label>

        <input type="radio" id="sedih" name="perasaan" value="Sedih" name="perasaan"
        <?php echo ($_SESSION['perasaan']=='Sedih')?'checked':'' ?>>

        <label for="sedih"
          style="float: left; clear: none; margin: 2px 0 0 2px; font-weight: unset;">&nbsp;Sedih</label>

        <input type="radio" id="marah" name="perasaan" value="Marah" name="perasaan"
        <?php echo ($_SESSION['perasaan']=='Marah')?'checked':'' ?>>

        <label for="marah"
          style="float: left; clear: none; margin: 2px 0 0 2px; font-weight: unset;">&nbsp;Marah</label>

      </div>
      <div class="inputfield">
        <label>Caption</label>
        <textarea
          style="resize:none; padding: 12px 15px; margin: 8px 0; width: 100%;"
          name="caption" rows="5" cols="5" name="caption" class="input"
          placeholder="Masukkan Caption"><?php echo $_SESSION['caption']; ?></textarea>
      </div>
      <div class="inputfield">
        <label>Foto</label>
        <input type="file" name="foto" id="foto" class="input">
      </div>
      <div class="image-preview" id="imagePreview">
        <img src="images/<?php echo $_SESSION['foto']; ?>" alt="Image Preview" class="image-preview__image">
        <span class="image-preview__default-text">Foto</span>
      </div>
      <div class="inputfield">
        <input type="hidden" name="id" value="<?php echo $_SESSION['id']; ?>">
        <input type="submit" value="Tambah" class="btn" name="edit2">

      </div>
    </form>
    </div>

    <script type="text/javascript">

  const inpFile = document.getElementById("foto");
		const previewContainer = document.getElementById("imagePreview");
		const previewImage = previewContainer.querySelector(".image-preview__image");
		const previewDefaultText = previewContainer.querySelector(".image-preview__default-text");
		
		inpFile.addEventListener("change", function() {
			const file = this.files[0];
			
			if(file){
			const reader = new FileReader();
			
			previewDefaultText.style.display = "none";
			previewImage.style.display = "block";
			
			reader.addEventListener("load", function() {
			previewImage.setAttribute("src", this.result);
			});
			
			reader.readAsDataURL(file);
			} 
			else{
			previewDefaultText.style.display = null;
			previewImage.style.display = null;
			previewImage.setAttribute("src", "images/<?php echo $_SESSION['foto']; ?>");
			}
		});
  </script>
</body>
</html>