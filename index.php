<?php
    session_start();

    $server_name = "localhost";
    $db_username = "root";
    $db_password = "";
    $db_name = "medsos";

    $connection = mysqli_connect($server_name,$db_username,$db_password,$db_name);
    $dbconfig = mysqli_select_db($connection,$db_name);
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Medsos</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
    integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js"
        integrity="sha384-xymdQtn1n3lH2wcu0qhcdaOpQwyoarkgLVxC/wZ5q7h9gHtxICrpcaSUfygqZGOe" crossorigin="anonymous">
    </script>
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link rel="stylesheet" href="style5.css">
</head>
<body>
    <br>
    <h1 class="text-center">Media Sosial</h1>
    <br>
    
    <table class="content-table">
        <?php

            $query = "select * from posting";
            $query_run = mysqli_query($connection, $query);
            
        ?>

        <thead>
            <tr>
            <th width="10%">ID</th>
            <th width="20%">Nama</th>
            <th width="20%">Perasaan</th>
            <th width="20%">Caption</th>
            <th width="20%">Foto</th>
            <th style="pointer-events: none; cursor: default;" width="200px">Edit</th>
            <th style="pointer-events: none; cursor: default;" width="200px">Delete</th>
            </tr>
        </thead>
        <tbody>

            <?php
            if(mysqli_num_rows($query_run) > 0)
            {
                while($row = mysqli_fetch_assoc($query_run))
                {
            ?>

            <tr>
            <td width="10%"><?php echo $row['id']; ?></td>
            <td width="20%">&nbsp;<?php echo $row['nama']; ?></td>
            <td width="20%">&nbsp;<?php echo $row['perasaan']; ?></td>
            <td width="20%">&nbsp;<?php echo $row['caption']; ?></td>
            <td width="20%">&nbsp;<img src="images/<?php echo $row['foto']; ?>" width="100"></td>
            <td width="200px">
                <form action="code.php" method="post">
                    <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
                    &nbsp;<button type="submit" name="edit" class="btn btn-success">Edit</button>
                </form>
            </td>
            <td width="200px">
                <form action="code.php" method="post">
                    <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
                    &nbsp;<button type="submit" name="hapus" class="btn btn-danger" onclick="return confirm('Yakin ingin hapus data?')">Delete</button>
                </form>
            </td>
            </tr>

            <?php
                }
            }
                
            else
            {
                echo '<h2 style="color: #f0134d;"> Tidak Ada Data </h2>';
            }
            ?>

        </tbody>
    </table>

    <div style="display: flex; justify-content: flex-end; align-items: center; margin-right: 10px;">
        <button onclick="window.location.href = 'tambah.php';" class="atma" style="margin-left: auto;">Tambah Data</button>
    </div>

    <script src="tablesort.js"></script>
</body>
</html>